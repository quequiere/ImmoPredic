package com.quequiere.predicimmo.simmulation;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.predicimmo.network.BatimentHelper;
import com.quequiere.predicimmo.object.Batiment;
import com.quequiere.predicimmo.object.QLabelBatiment;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QTableWidget;

public class HistoryLocation
{

	private double startMoney = -1;
	// <day,map<nom,quantity>>
	private HashMap<Integer, HashMap<String, Integer>> histo = new HashMap<Integer, HashMap<String, Integer>>();

	public double getStartMoney()
	{
		return startMoney;
	}

	public void setStartMoney(double startMoney)
	{
		this.startMoney = startMoney;
	}

	public int getInPeriod(int day)
	{
		int best = -1;

		for (Integer i : histo.keySet())
		{
			if (i >= best && i <= day)
			{
				best = i;
			}
		}

		return best;
	}

	public HashMap<String, Integer> getDifferenceWithPeriod(int last, int current)
	{
		HashMap<String, Integer> differences = new HashMap<String, Integer>();

		HashMap<String, Integer> cmap = histo.get(current);
		HashMap<String, Integer> lmap = histo.get(last);

		for (String name : lmap.keySet())
		{
			int currentValue = cmap.get(name);
			int lastValue = lmap.get(name);

			if (currentValue != lastValue)
			{
				int diffValue = currentValue - lastValue;
				differences.put(name, diffValue);
			}

		}

		return differences;
	}

	public HashMap<String, Integer> getQuantityForDay(int day) // 0 5 |17| 25
	{
		return histo.get(this.getInPeriod(day));
	}

	public void saveData(int day, ArrayList<QTableWidget> tablesSave)
	{
		if (histo.get(day) != null)
		{
			histo.remove(day);
		}

		HashMap<String, Integer> data = new HashMap<String, Integer>();

		for (QTableWidget table : tablesSave)
		{
			for (int x = 0; x < table.rowCount(); x++)
			{
				QLabelBatiment name = (QLabelBatiment) table.cellWidget(x, 1);
				Batiment b = name.getBatiment();
				int number = Integer.parseInt(((QLabel) table.cellWidget(x, 3)).text());
				data.put(b.getName(), number);
			}
		}

		histo.put(day, data);

	}

	public double getBeneficeLoyerForDay(int day)
	{
		int periode = this.getInPeriod(day);
		double benef = 0;
		HashMap<String, Integer> map = histo.get(periode);

		for (String name : map.keySet())
		{
			int quantity = map.get(name);
			if (quantity == 0)
				continue;

			Batiment b = BatimentHelper.getByName(name);
			benef += (b.getFinalLoyer() * quantity);
		}

		return benef;
	}

	public double calculBalance(double startBalance, double targetDay, double otherIncomePerDay)
	{
		
		double balance = startBalance;
		int lastPeriode = 0;

		for (int x = 0; x < targetDay; x++)
		{
			int periode = this.getInPeriod(x);
			balance += (this.getBeneficeLoyerForDay(periode) + otherIncomePerDay);

			
			// Module achat et vente ====================================================
			//on ajoute de l'avance pour compter immediatement de debit contrairement au loyer qui ne sera pris en compte que le lendemain
			 periode = this.getInPeriod(x+1);
			
			if (lastPeriode != periode)
			{
				
				// Changement de periode, on calcul les achats et les ventes !
				HashMap<String, Integer> map = getDifferenceWithPeriod(lastPeriode, periode);
				for (String name : map.keySet())
				{
					int quantity = map.get(name);
					if (quantity == 0)
						continue;

					Batiment b = BatimentHelper.getByName(name);

					if (quantity < 0)
					{
						// on fait donc une vente
						balance+= ( b.getPriceVente()*-quantity);
					}
					else if (quantity > 0)
					{
						// on fait un achat
						balance-= (b.getPriceAchat());
					}
				}

			}
			
			
			//=======================================

			lastPeriode = periode;
		}

		return balance;
	}

}
