package com.quequiere.predicimmo.log;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import com.quequiere.predicimmo.MainC;

public class MyOutputStream extends FilterOutputStream
{
	String s = "";

	public MyOutputStream(OutputStream out)
	{
		super(out);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{

		String str = new String(b, StandardCharsets.UTF_8);
		s += str;

		String newline = System.getProperty("line.separator");

		if (s.contains(newline))
		{
			//MainC.index.textEdit.append(s);
			s = "";
		}

		super.write(b, off, len);
	}

}