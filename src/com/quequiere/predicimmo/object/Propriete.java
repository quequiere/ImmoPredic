package com.quequiere.predicimmo.object;

public class Propriete
{
	private Batiment batiment;
	private int id;
	private double etat;
	private StatutType statut;
	
	
	public Propriete(Batiment batiment, int id, double etat, StatutType statut)
	{
		this.batiment = batiment;
		this.id = id;
		this.etat = etat;
		this.statut = statut;
		
	}
	public Batiment getBatiment()
	{
		return batiment;
	}
	
	public int getId()
	{
		return id;
	}
	public double getEtat()
	{
		return etat;
	}
	public StatutType getStatut()
	{
		return statut;
	}
	
	

}
