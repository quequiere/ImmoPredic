package com.quequiere.predicimmo.object;

import java.util.ArrayList;


import com.quequiere.predicimmo.network.LoginHelper;
import com.quequiere.predicimmo.network.ProprieteHelper;

public class Profile
{

	public static boolean isEntreprise = false;
	
	
	private String username;
	private Long fond;
	private int points;
	private int place;
	private int level;
	private long caisseSolidaire;
	private long pret;
	private ArrayList<Propriete> proprietes = new ArrayList<Propriete>();

	public Profile(String username)
	{
		this.username = username;
	}
	
	
	

	public long getPret() {
		return pret;
	}




	public void setPret(long pret) {
		this.pret = pret;
	}




	public ArrayList<Propriete> getProprietes()
	{
		return proprietes;
	}



	public void reloadPropriete()
	{
		proprietes=ProprieteHelper.reloadPropriete();
		System.out.println(proprietes.size()+" proprieties loaded !");
	}
	
	public long getCaisseSolidaire()
	{
		return caisseSolidaire;
	}


	public void setCaisseSolidaire(long caisseSolidaire)
	{
		this.caisseSolidaire = caisseSolidaire;
	}


	public int getPlace()
	{
		return place;
	}

	public void setPlace(int place)
	{
		this.place = place;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public void setPoints(int points)
	{
		this.points = points;
	}

	public void setFond(Long fond)
	{
		this.fond = fond;
	}

	public String getUsername()
	{
		return username;
	}

	public Long getFond()
	{
		return fond;
	}

	public int getPoints()
	{
		return points;
	}

	public boolean connect(String password)
	{
		return LoginHelper.connect(this.getUsername(), password);
	}

}
