package com.quequiere.predicimmo.object;

import com.trolltech.qt.gui.QLabel;

public class QLabelBatiment extends QLabel
{
	private Batiment batiment;
	
	public QLabelBatiment(Batiment b)
	{
		super();
		super.setText(b.getName());
		this.batiment=b;
	}
	
	public Batiment getBatiment()
	{
		return batiment;
	}
	public void setBatiment(Batiment batiment)
	{
		this.batiment = batiment;
		this.setText(this.batiment.getName());
	}
	
	

	
}
