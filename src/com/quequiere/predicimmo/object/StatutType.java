package com.quequiere.predicimmo.object;

public enum StatutType
{
	Entravaux,
	Enlocation,
	Innactif;
}
