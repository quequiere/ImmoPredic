package com.quequiere.predicimmo.object;

public enum BatimentTypeEntreprise
{
	bureaux,
	commerciaux,
	industriels,
	transport,
	zones;
}
