package com.quequiere.predicimmo.object;

import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QPalette;

public class QLabelColored extends QLabel
{
	public QLabelColored(String s,QPalette qpallette)
	{
		super(s);
		this.setPalette(qpallette);
	}

}
