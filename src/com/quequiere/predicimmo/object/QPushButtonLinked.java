package com.quequiere.predicimmo.object;

import com.quequiere.predicimmo.gui.windows.ProprietesWindow;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QPushButton;

public class QPushButtonLinked extends QPushButton
{
	private QLabel linked;
	private ProprietesWindow proprietesWindow;
	
	public QPushButtonLinked (String s,QLabel labelLinked, ProprietesWindow proprietesWindow)
	{
		super(s);
		this.linked=labelLinked;
		this.proprietesWindow=proprietesWindow;
	}
	
	public void add()
	{
		Integer i = Integer.parseInt(linked.text());
		i++;
		linked.setText(""+i);
		proprietesWindow.recalculInformation();
	}
	
	public void remove()
	{
		Integer i = Integer.parseInt(linked.text());
		i--;
		if(i>=0)
		{
			linked.setText(""+i);
		}
		
		proprietesWindow.recalculInformation();
		
	}

}
