package com.quequiere.predicimmo.gui.windows;

import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QWidget;

public class FinanceOptionWindow extends QWidget
{

	private String rsrcPath = "classpath:ressources";

	public FinanceOptionWindow()
	{
		QGridLayout layout = new QGridLayout();
		QGroupBox loginGroup = new QGroupBox(tr("R�glage des finances"));
		layout.addWidget(loginGroup, 0, 0);
		setLayout(layout);
	}

}
