package com.quequiere.predicimmo.gui.windows.topinvest;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import com.quequiere.predicimmo.calculator.loyer.CalculatorLoyer;
import com.quequiere.predicimmo.calculator.loyer.Loyer;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.object.QLabelColored;
import com.quequiere.predicimmo.tools.ToolsJambi;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.core.Qt.AlignmentFlag;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QFont;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QHBoxLayout;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QPalette;
import com.trolltech.qt.gui.QScrollArea;
import com.trolltech.qt.gui.QSlider;
import com.trolltech.qt.gui.QTabWidget;
import com.trolltech.qt.gui.QVBoxLayout;
import com.trolltech.qt.gui.QWidget;
import com.trolltech.qt.gui.QPalette.ColorRole;

public class TopInvestissement extends QWidget
{
// AJOUTER SYST7ME SIMULATION A X JOURS
	private String rsrcPath = "classpath:ressources";



	public TopInvestissement()
	{
		QTabWidget tabwidget = new QTabWidget();
		tabwidget.setTabsClosable(false);

		// =============== Initilisation de la tab

		QTabWidgetOngleLoyer tab1 = new QTabWidgetOngleLoyer();
		tabwidget.addTab(tab1, "Top location");

		
		// ======Fin tab 1==================================

		QWidget tab2 = new QWidget();
		tabwidget.addTab(tab2, "Top embellissement");
		QWidget tab3 = new QWidget();
		tabwidget.addTab(tab3, "Top construction");
		QWidget tab4 = new QWidget();
		tabwidget.addTab(tab4, "Top ench�res");

		QHBoxLayout layout = new QHBoxLayout();
		layout.addWidget(tabwidget);
		setLayout(layout);

		
		
		
	}

	
}
