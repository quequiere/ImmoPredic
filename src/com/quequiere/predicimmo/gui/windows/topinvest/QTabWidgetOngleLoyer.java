package com.quequiere.predicimmo.gui.windows.topinvest;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import com.quequiere.predicimmo.calculator.loyer.CalculatorLoyer;
import com.quequiere.predicimmo.calculator.loyer.Loyer;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.object.QLabelColored;
import com.quequiere.predicimmo.tools.ToolsJambi;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.core.Qt.AlignmentFlag;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QFont;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QHBoxLayout;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QScrollArea;
import com.trolltech.qt.gui.QSlider;
import com.trolltech.qt.gui.QVBoxLayout;
import com.trolltech.qt.gui.QWidget;

public class QTabWidgetOngleLoyer extends QWidget
{
	private QLabel tab1filtreMaxAffichage, tab1investissementMaxAffichage,filtreJourSimule;
	private QSlider sliderTab1DisplayMax, sliderTab1MaxInvest,sliderJourSimule;
	private QVBoxLayout layoutTab1, layoutTab1Liste;
	
	public QTabWidgetOngleLoyer()
	{
		super();
		
		QHBoxLayout tab1hbox = new QHBoxLayout();
		this.setLayout(tab1hbox);

		QScrollArea scrollArea = new QScrollArea();
		scrollArea.setWidgetResizable(true);
		tab1hbox.addWidget(scrollArea);

		QWidget centralWidget = new QWidget();
		layoutTab1 = new QVBoxLayout(centralWidget);

		QWidget centralWidgetForListe = new QWidget();
		layoutTab1Liste = new QVBoxLayout(centralWidgetForListe);

		QLabelColored label = new QLabelColored("Classement des meilleures locations", MainPannel.rouge);
		label.setFont(new QFont("Times", 18, QFont.Weight.Bold.value()));
		label.setAlignment(AlignmentFlag.AlignCenter);
		layoutTab1.addWidget(label);

		QGroupBox groupParam = new QGroupBox(tr("Param�tres et filtres"));
		ToolsJambi.setColorGroup(groupParam, QColor.white);
		layoutTab1.addWidget(groupParam);
		QGridLayout gridParam = new QGridLayout();
		groupParam.setLayout(gridParam);

		// ======Option1
		tab1filtreMaxAffichage = new QLabel("Maximum � afficher (X)");
		gridParam.addWidget(tab1filtreMaxAffichage, 0, 0);

		sliderTab1DisplayMax = new QSlider(Qt.Orientation.Horizontal);
		sliderTab1DisplayMax.setRange(5, 100);
		sliderTab1DisplayMax.setValue(10);
		sliderTab1DisplayMax.valueChanged.connect(this, "refreshTab1()");
		sliderTab1DisplayMax.sliderReleased.connect(this, "refreshTab1Release()");
		gridParam.addWidget(sliderTab1DisplayMax, 0, 1);

		// ====================

		// ======Option2
		tab1investissementMaxAffichage = new QLabel("Investissement maximum (X)");
		gridParam.addWidget(tab1investissementMaxAffichage, 1, 0);

		sliderTab1MaxInvest = new QSlider(Qt.Orientation.Horizontal);
		sliderTab1MaxInvest.setRange(0, 100000000);
		sliderTab1MaxInvest.setValue(100000000);
		sliderTab1MaxInvest.valueChanged.connect(this, "refreshTab1()");
		sliderTab1MaxInvest.sliderReleased.connect(this, "refreshTab1Release()");
		gridParam.addWidget(sliderTab1MaxInvest, 1, 1);
		
		
		filtreJourSimule = new QLabel("Jour simul� (X)");
		gridParam.addWidget(filtreJourSimule, 2, 0);
		
		sliderJourSimule = new QSlider(Qt.Orientation.Horizontal);
		sliderJourSimule.setRange(0, 365);
		sliderJourSimule.setValue(0);
		sliderJourSimule.valueChanged.connect(this, "refreshTab1()");
		sliderJourSimule.sliderReleased.connect(this, "refreshTab1Release()");
		gridParam.addWidget(sliderJourSimule, 2, 1);

		// ====================

		layoutTab1.addWidget(centralWidgetForListe);
		scrollArea.setWidget(centralWidget);
		
		this.refreshTab1();
		this.refreshTab1Release();

	}
	
	public void refreshTab1Release()
	{

		ToolsJambi.clearGrid(layoutTab1Liste);


		ArrayList<Loyer> toDisplay = new ArrayList<Loyer>();

		for (Loyer l : CalculatorLoyer.getLoyer())
		{
			int maxInvest = sliderTab1MaxInvest.value();

			if (l.getInvestissement() <= maxInvest || maxInvest == sliderTab1MaxInvest.maximum())
			{
				if (l.getDailyIncome() > 0)
				{
					toDisplay.add(l);
				}

			}
		}

		Collections.sort(toDisplay, new Comparator<Loyer>()
		{
			@Override
			public int compare(Loyer c1, Loyer c2)
			{
				return Double.compare(c1.getTimeBeforeRefund(), c2.getTimeBeforeRefund());
			}
		});

		int x = 0;
		for (Loyer l : toDisplay)
		{
			if (x >= sliderTab1DisplayMax.value())
				break;

			QGroupBox groupParam = new QGroupBox(tr(l.getBatiment().getName()));
			groupParam.setContentsMargins(5, 5, 5, 10);

			ToolsJambi.setColorGroup(groupParam, QColor.white);
			layoutTab1Liste.addWidget(groupParam);

			QGridLayout grid = new QGridLayout();

			QLabel label = new QLabelColored("Temps avant remboursement: " + l.getTimeBeforeRefund() + " jours IRL", MainPannel.vert);
			
			
			label.setFont(new QFont("Times", 13, QFont.Weight.Bold.value()));
			label.setAlignment(AlignmentFlag.AlignHCenter);
			grid.addWidget(label, 0, 0, 1, 2);

			label = new QLabel("Loyer par jour TTC: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(l.getDailyIncome()));
			label.setAlignment(AlignmentFlag.AlignHCenter);
			grid.addWidget(label, 1, 0);

			label = new QLabel("Investissement: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(l.getInvestissement()));
			label.setAlignment(AlignmentFlag.AlignHCenter);
			grid.addWidget(label, 1, 1);
			
			double montantGain = (l.getDailyIncome()*sliderJourSimule.value())-l.getInvestissement();
			label = new QLabelColored("Montant gagn� � J+"+sliderJourSimule.value()+ " ==> " + NumberFormat.getNumberInstance(Locale.FRENCH).format(montantGain),MainPannel.bleu);
			label.setAlignment(AlignmentFlag.AlignHCenter);
			grid.addWidget(label, 2, 0,1,2);

			groupParam.setLayout(grid);

			x++;
		}

	}

	public void refreshTab1()
	{
		tab1investissementMaxAffichage.setText("Investissement maximum " + NumberFormat.getNumberInstance(Locale.FRENCH).format(sliderTab1MaxInvest.value()));

		tab1filtreMaxAffichage.setText("Maximum � afficher " + sliderTab1DisplayMax.value());
		
		filtreJourSimule.setText("Jour simul� J+ " + sliderJourSimule.value());

	}

}
