package com.quequiere.predicimmo.gui.windows;

import java.text.NumberFormat;
import java.util.Locale;

import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.network.BatimentHelper;
import com.quequiere.predicimmo.network.LoginHelper;
import com.trolltech.qt.core.Qt.AlignmentFlag;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QHBoxLayout;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QPixmap;
import com.trolltech.qt.gui.QSizePolicy.Policy;
import com.trolltech.qt.gui.QTextEdit;
import com.trolltech.qt.gui.QWidget;

public class IndexBox extends QWidget
{
	private String rsrcPath = "classpath:ressources";

	private QPixmap tirelireF = new QPixmap(rsrcPath + "/tirelire.png");
	private QPixmap profileF = new QPixmap(rsrcPath + "/profile.png");
	private QPixmap pointsF = new QPixmap(rsrcPath + "/points.png");
	private QPixmap placeF = new QPixmap(rsrcPath + "/place.png");
	
	
	private QLabel fond;
	private QLabel points;
	private QLabel place;
	private QLabel niveau;
	
	public QTextEdit textEdit = new QTextEdit(); 

	public IndexBox(boolean reloadData)
	{
		
		
		QGroupBox loginGroup = new QGroupBox(tr("Vos informations actuelles:"));

		fond = new QLabel(tr("Fonds: --- "));
		QLabel fondImg = new QLabel();
		fondImg.setPixmap(tirelireF);
		fondImg.setAlignment(AlignmentFlag.AlignRight,AlignmentFlag.AlignCenter);
		
		

		
		points = new QLabel(tr("Points: ---" ));
		QLabel pointsImg = new QLabel();
		pointsImg.setPixmap(pointsF);
		pointsImg.setAlignment(AlignmentFlag.AlignRight,AlignmentFlag.AlignCenter);
		
		
		
		place = new QLabel(tr("Place : ---"));
		QLabel placeImg = new QLabel();
		placeImg.setPixmap(placeF);
		placeImg.setAlignment(AlignmentFlag.AlignRight,AlignmentFlag.AlignCenter);
	
		
		
		niveau = new QLabel(tr("Niveau : ---"));
		QLabel niveauImg = new QLabel();
		niveauImg.setPixmap(placeF);
		niveauImg.setAlignment(AlignmentFlag.AlignRight,AlignmentFlag.AlignCenter);


		

		QHBoxLayout loginLayout = new QHBoxLayout();
		
		loginLayout.addStretch();
		
		loginLayout.addWidget(fondImg);
		loginLayout.addWidget(fond);
		
		loginLayout.addStretch();
		
		loginLayout.addWidget(pointsImg);
		loginLayout.addWidget(points);
		
		loginLayout.addStretch();
		
		loginLayout.addWidget(placeImg);
		loginLayout.addWidget(place);
		
		loginLayout.addStretch();
		
		loginLayout.addWidget(niveauImg);
		loginLayout.addWidget(niveau);
		
		loginLayout.addStretch();

		loginGroup.setLayout(loginLayout);
		

		QGridLayout layout = new QGridLayout();
		
		layout.addWidget(loginGroup, 0, 0);
		layout.addWidget(textEdit, 1, 0);
	
		setLayout(layout);
		
		if(reloadData)
		{
			this.reloadData();
		}
	
				
	}
	
	public void reloadData()
	{
		fond.setText("Fonds: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(MainC.profile.getFond()) + " �");
		points.setText("Points: " + MainC.profile.getPoints());
		place.setText("Place : " + MainC.profile.getPlace());
		niveau.setText("Niveau : " + MainC.profile.getLevel());
	}

}
