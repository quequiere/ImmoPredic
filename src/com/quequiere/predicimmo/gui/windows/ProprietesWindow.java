package com.quequiere.predicimmo.gui.windows;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.network.BatimentHelper;
import com.quequiere.predicimmo.object.Batiment;
import com.quequiere.predicimmo.object.BatimentType;
import com.quequiere.predicimmo.object.Propriete;
import com.quequiere.predicimmo.object.QLabelBatiment;
import com.quequiere.predicimmo.object.QLabelColored;
import com.quequiere.predicimmo.object.QPushButtonLinked;
import com.quequiere.predicimmo.simmulation.HistoryLocation;
import com.quequiere.predicimmo.tools.ToolsJambi;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.core.Qt.AlignmentFlag;
import com.trolltech.qt.gui.QAbstractItemView.SelectionBehavior;
import com.trolltech.qt.gui.QAbstractItemView.SelectionMode;
import com.trolltech.qt.gui.QCheckBox;
import com.trolltech.qt.gui.QFont;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QLayoutItem;
import com.trolltech.qt.gui.QLayoutItemInterface;
import com.trolltech.qt.gui.QLineEdit;
import com.trolltech.qt.gui.QPushButton;
import com.trolltech.qt.gui.QSlider;
import com.trolltech.qt.gui.QTableWidget;
import com.trolltech.qt.gui.QWidget;

public class ProprietesWindow extends QWidget
{

	private String rsrcPath = "classpath:ressources";

	private ArrayList<QTableWidget> tablesSave;
	private QGridLayout generalGroupGridSave;

	private QLabelColored labelDate;
	private int currentDate = 0;
	private int lastPeriod = -1;

	private QPushButton saveButton;

	private QLineEdit startupMoney;
	private QLabelColored balancePrevisionelle;

	private QPushButton simulerButton;
	private QCheckBox useAidesEmprunt;

	public HistoryLocation history = new HistoryLocation();

	private QSlider slider;

	private QGroupBox notificationBox;

	public ProprietesWindow()
	{
		MainC.profile.reloadPropriete();
		QGridLayout layout = new QGridLayout();

		QGroupBox groupLayout = new QGroupBox(tr("R�glage des propri�t�"));
		layout.addWidget(groupLayout, 0, 0);
		setLayout(layout);

		QGridLayout typesGroupGrid = new QGridLayout();
		int x = 2;
		int y = 0;

		labelDate = new QLabelColored("Jour simul�: J " + NumberFormat.getNumberInstance(Locale.FRENCH).format(0), MainPannel.rouge);
		labelDate.setFont(new QFont("Times", 18, QFont.Weight.Bold.value()));
		labelDate.setAlignment(AlignmentFlag.AlignCenter);
		typesGroupGrid.addWidget(labelDate, 0, 0, 1, 0);

		notificationBox = new QGroupBox(tr("Notification"));
		notificationBox.setVisible(false);
		typesGroupGrid.addWidget(notificationBox, 1, 0, 1, 0);

		ArrayList<QTableWidget> tables = new ArrayList<QTableWidget>();

		for (BatimentType type : BatimentType.values())
		{
			ArrayList<Batiment> concern = new ArrayList<Batiment>();

			QGroupBox groupBox = new QGroupBox(tr(type.name() + " :"));
			typesGroupGrid.addWidget(groupBox, x, y);

			if (y == 0)
			{
				y++;
			}
			else
			{
				y = 0;
				x++;
			}

			QGridLayout localGrid = new QGridLayout();
			groupBox.setLayout(localGrid);

			QTableWidget tableWidget = new QTableWidget();
			tables.add(tableWidget);
			localGrid.addWidget(tableWidget);
			tableWidget.setColumnCount(4);
			tableWidget.setShowGrid(false);
			tableWidget.setSelectionMode(SelectionMode.SingleSelection);
			tableWidget.setSelectionBehavior(SelectionBehavior.SelectRows);
			tableWidget.verticalHeader().setVisible(false);
			tableWidget.horizontalHeader().setVisible(false);

			for (Batiment b : BatimentHelper.batiments)
			{
				if (!b.getType().equals(type))
					continue;
				concern.add(b);
			}

			tableWidget.setRowCount(concern.size());
			int ligneInterne = 0;
			for (Batiment b : concern)
			{

				QLabelBatiment label = new QLabelBatiment(b);
				label.setAlignment(AlignmentFlag.AlignCenter);
				tableWidget.setCellWidget(ligneInterne, 1, label);

				QLabel quantity = new QLabel("0");
				quantity.setAlignment(AlignmentFlag.AlignCenter);
				tableWidget.setCellWidget(ligneInterne, 3, quantity);

				QPushButtonLinked buttonm = new QPushButtonLinked("-", quantity, this);
				buttonm.clicked.connect(buttonm, "remove()");
				buttonm.setFixedWidth(25);
				tableWidget.setCellWidget(ligneInterne, 0, buttonm);

				QPushButtonLinked buttonp = new QPushButtonLinked("+", quantity, this);
				buttonp.clicked.connect(buttonp, "add()");
				buttonp.setFixedWidth(25);
				tableWidget.setCellWidget(ligneInterne, 2, buttonp);

				ligneInterne++;

			}

			tableWidget.resizeRowsToContents();
			tableWidget.resizeColumnsToContents();

			tableWidget.setColumnWidth(0, 26);
			tableWidget.setColumnWidth(2, 26);

		}

		// ====================== INFO====================

		QGroupBox typesGroup = new QGroupBox(tr("Informations"));
		typesGroupGrid.addWidget(typesGroup, 4, 0, 4, -1);
		groupLayout.setLayout(typesGroupGrid);

		QGroupBox generalGroup = new QGroupBox(tr("Information courrante"));
		QGroupBox selectionGroup = new QGroupBox(tr("Pr�vision J -"));
		QGroupBox actionGroup = new QGroupBox(tr("Action"));

		// ============ PREVISION BAR
		QGridLayout gridPrevispion = new QGridLayout();

		balancePrevisionelle = new QLabelColored("Balance pr�visionnelle � J " + currentDate + ": ", MainPannel.bleu);
		balancePrevisionelle.setFont(new QFont("Times", 12, QFont.Weight.Bold.value()));
		gridPrevispion.addWidget(balancePrevisionelle, 0, 0);

		//simulerButton = new QPushButton("Simuler", this);
		//simulerButton.clicked.connect(this, "simuler()");
		//simulerButton.setFixedWidth(100);
		//simulerButton.setEnabled(false);
		//gridPrevispion.addWidget(simulerButton, 0, 1);

		selectionGroup.setLayout(gridPrevispion);

		// =========================

		QGridLayout gridGroupAction = new QGridLayout();

		slider = new QSlider(Qt.Orientation.Horizontal);
		slider.setRange(0, 365);
		slider.setValue(0);
		slider.valueChanged.connect(this, "slideDay(int)");
		slider.setEnabled(false);
		gridGroupAction.addWidget(slider, 1, 0);
		
		
		
		useAidesEmprunt = new QCheckBox("D�sactiver emprunts et aides");
		gridGroupAction.addWidget(useAidesEmprunt, 1, 1);
		

		saveButton = new QPushButton("Sauvegarder la journ�e", this);
		saveButton.clicked.connect(this, "saveHistory()");
		saveButton.setFixedWidth(160);
		saveButton.setEnabled(true);
		gridGroupAction.addWidget(saveButton, 2, 0);

		QLabelColored startupLabel = new QLabelColored("Argent de d�part:", MainPannel.bleu);
		gridGroupAction.addWidget(startupLabel, 2, 1);

		startupMoney = new QLineEdit();
		startupMoney.setText(MainC.profile.getFond() + "");
		gridGroupAction.addWidget(startupMoney, 2, 2);

		actionGroup.setLayout(gridGroupAction);

		QGridLayout gridGroupInfo = new QGridLayout();
		gridGroupInfo.addWidget(generalGroup, 0, 0, 0, 1);
		gridGroupInfo.addWidget(selectionGroup, 0, 1);
		gridGroupInfo.addWidget(actionGroup, 1, 1);

		typesGroup.setLayout(gridGroupInfo);

		QGridLayout generalGroupGrid = new QGridLayout();

		generalGroup.setLayout(generalGroupGrid);

		this.generalGroupGridSave = generalGroupGrid;
		this.tablesSave = tables;

		reloadTableFromProprieties(tables, generalGroupGrid);

	}

	public void recalculInformation()
	{
		ToolsJambi.clearGrid(generalGroupGridSave);

		double charge = 0;
		double impot = 0;
		double loyer = 0;
		int total = 0;
		for (QTableWidget table : this.tablesSave)
		{
			for (int x = 0; x < table.rowCount(); x++)
			{
				QLabelBatiment name = (QLabelBatiment) table.cellWidget(x, 1);
				Batiment b = name.getBatiment();
				int number = Integer.parseInt(((QLabel) table.cellWidget(x, 3)).text());
				if (number <= 0)
					continue;
				total += number;
				charge += (b.getCharges() * number);
				impot += (b.getImpots() * number);
				loyer = loyer + (b.getLoyer() * number);
			}
		}

		generalGroupGridSave.addWidget(new QLabel("Nombre de propri�t� r�elles: " + MainC.profile.getProprietes().size()), 0, 0);
		QLabelColored color = new QLabelColored("Nombre de propri�t� simul�es: " + total, MainPannel.bleu);
		color.setAlignment(AlignmentFlag.AlignCenter);
		generalGroupGridSave.addWidget(color, 0, 1);
		//generalGroupGridSave.addWidget(new QLabelColored("Charges journali�res: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(charge), MainPannel.rouge), 1, 0);
		//generalGroupGridSave.addWidget(new QLabelColored("Impots journaliers: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(impot), MainPannel.rouge), 1, 1);
		generalGroupGridSave.addWidget(new QLabelColored("Emprunts: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(MainC.profile.getPret()), MainPannel.rouge), 1, 0);
		//generalGroupGridSave.addWidget(new QLabelColored("Loyers vers�s: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(loyer), MainPannel.vert), 2, 0);

		double autreRevenu = MainC.profile.getCaisseSolidaire();

		generalGroupGridSave.addWidget(new QLabelColored("Revenus autres: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(autreRevenu), MainPannel.vert), 1, 1);

	}

	public void reloadTableFromProprieties(ArrayList<QTableWidget> tables, QGridLayout generalGroupGrid)
	{
		HashMap<Batiment, Integer> quantity = new HashMap<Batiment, Integer>();

		for (Propriete p : MainC.profile.getProprietes())
		{
			if (!quantity.containsKey(p.getBatiment()))
			{
				quantity.put(p.getBatiment(), 0);
			}

			int last = quantity.get(p.getBatiment());
			quantity.remove(p.getBatiment());
			last++;
			quantity.put(p.getBatiment(), last);
		}

		for (QTableWidget table : tables)
		{
			for (int x = 0; x < table.rowCount(); x++)
			{
				QLabelBatiment name = (QLabelBatiment) table.cellWidget(x, 1);

				if (quantity.containsKey(name.getBatiment()))
				{
					QLabel qtlab = (QLabel) table.cellWidget(x, 3);
					qtlab.setText("" + quantity.get(name.getBatiment()));

				}

			}

		}

		recalculInformation();
	}

	public void saveHistory()
	{
		if (currentDate == 0)
		{
			double startMoney = 0;
			try
			{
				startMoney = Double.parseDouble(startupMoney.text());
				history.setStartMoney(startMoney);
				history.saveData(currentDate, tablesSave);
				slider.setEnabled(true);
			}
			catch (NumberFormatException e)
			{
				System.out.println("Erreur de montant de la start money !");
			}
		}
		else
		{
			history.saveData(currentDate, tablesSave);
		}

	}

	public void simuler()
	{

	}

	public void slideDay(int i)
	{
		currentDate = i;
		int currentPeriod = history.getInPeriod(i);

		labelDate.setText("Jour simul�: J " + NumberFormat.getNumberInstance(Locale.FRENCH).format(currentDate));

		if (currentDate == 0)
		{
			this.saveButton.setEnabled(false);
			//simulerButton.setEnabled(false);
		}
		else
		{
			this.saveButton.setEnabled(true);
			//simulerButton.setEnabled(true);
			startupMoney.setEnabled(false);
		}

		HashMap<String, Integer> map = history.getQuantityForDay(currentDate);

		for (QTableWidget table : this.tablesSave)
		{
			for (int x = 0; x < table.rowCount(); x++)
			{
				QLabelBatiment name = (QLabelBatiment) table.cellWidget(x, 1);
				QLabel label = (QLabel) table.cellWidget(x, 3);
				label.setText(map.get(name.text()) + "");
			}
		}

		// ==================Balance calcul

		double autreRevenu = MainC.profile.getCaisseSolidaire() - MainC.profile.getPret();
		
		if(useAidesEmprunt.isChecked())
			autreRevenu=0;
		
		double balSim = history.calculBalance(Double.parseDouble(startupMoney.text()), currentDate, autreRevenu);
		balancePrevisionelle.setText("Balance pr�visionnelle � J+ " + currentDate + ":   "+NumberFormat.getNumberInstance(Locale.FRENCH).format(balSim));
	
		// =======================

		// ======== NOTIFICATION BOX

		if (currentPeriod != lastPeriod)
		{

			QGridLayout grid = (QGridLayout) notificationBox.layout();
			if (notificationBox.layout() == null)
			{
				grid = new QGridLayout();
				notificationBox.setLayout(grid);
			}
			ToolsJambi.clearGrid(grid);

			int periodeReference = history.getInPeriod(currentPeriod - 1);

			if (periodeReference >= 0)
			{
				notificationBox.setVisible(true);
				QLabelColored label = new QLabelColored("Modification par rapport � J+ " + periodeReference + " VS J+ " + currentPeriod, MainPannel.bleu);
				label.setFont(new QFont("Times", 10, QFont.Weight.Bold.value()));
				label.setAlignment(AlignmentFlag.AlignCenter);
				grid.addWidget(label, 0, 0, 1, 3);

				HashMap<String, Integer> difMap = history.getDifferenceWithPeriod(periodeReference, currentPeriod);

				int lig = 1;
				int col = 0;
				for (String name : difMap.keySet())
				{
					int value = difMap.get(name);
					Batiment b = BatimentHelper.getByName(name);
					if (value < 0)
					{
					
						label = new QLabelColored(name + " " + value+" (+"+ NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getPriceVente())+")", MainPannel.rouge);
					}
					else
					{
						label = new QLabelColored(name + " +" + value+" (-"+ NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getPriceAchat())+")", MainPannel.vert);
					}

					grid.addWidget(label, lig, col);
					col++;

					if (col == 3)
					{
						col = 0;
						lig++;
					}

				}

			}
			else
			{
				notificationBox.setVisible(false);
			}

		}

		lastPeriod = currentPeriod;

		// ====================

	}

}
