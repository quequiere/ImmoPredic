package com.quequiere.predicimmo.gui;

import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.gui.login.LoginBox;
import com.quequiere.predicimmo.gui.windows.FinanceOptionWindow;
import com.quequiere.predicimmo.gui.windows.IndexBox;
import com.quequiere.predicimmo.gui.windows.ProprietesWindow;
import com.quequiere.predicimmo.gui.windows.topinvest.TopInvestissement;
import com.trolltech.qt.gui.QMainWindow;
import com.trolltech.qt.gui.QMenu;
import com.trolltech.qt.gui.QToolBar;
import com.trolltech.qt.gui.QPalette.ColorRole;
import com.trolltech.qt.gui.QAction;
import com.trolltech.qt.gui.QMenuBar;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QPalette;
import com.trolltech.qt.gui.QIcon;
import com.trolltech.qt.gui.QKeySequence;

public class MainPannel extends QMainWindow
{

	public static QPalette rouge = new QPalette();
	public static QPalette vert = new QPalette();
	public static QPalette bleu = new QPalette();

	private String rsrcPath = "classpath:ressources";
	
	private QMenu fileMenu;
	private QMenu helpMenu;
	
	private QToolBar fileToolBar;

	private QAction financeOption;
	private QAction situation;
	private QAction properties;
	private QAction topInvestissement;
	
	private QAction aboutAct;
	private QAction aboutQtAct;

	public MainPannel()
	{
		rouge.setColor(ColorRole.WindowText, QColor.red);
		vert.setColor(ColorRole.WindowText, QColor.darkGreen);
		bleu.setColor(ColorRole.WindowText, QColor.blue);

		QMenuBar menuBar = new QMenuBar();
		setMenuBar(menuBar);

		LoginBox lb = new LoginBox();
		setCentralWidget(lb);

		createActions();
		createMenus();
		createToolBars();
		createStatusBar();
		setUnifiedTitleAndToolBarOnMac(true);
	}


	private void createActions()
	{

		financeOption = new QAction(new QIcon(rsrcPath + "/finance.png"), tr("&Finances"), this);
		financeOption.setShortcut(new QKeySequence(tr("Ctrl+N")));
		financeOption.setStatusTip(tr("R�gler les finances"));
		financeOption.triggered.connect(this,"displayFinance()");
		financeOption.setEnabled(false);
		
		situation = new QAction(new QIcon(rsrcPath + "/situation.png"), tr("&Situation"), this);
		situation.setShortcut(new QKeySequence(tr("Ctrl+S")));
		situation.setStatusTip(tr("Situation et logs"));
		situation.triggered.connect(this,"displaySituation()");
		situation.setEnabled(false);
		
		properties = new QAction(new QIcon(rsrcPath + "/properties.png"), tr("&Propri�t�s"), this);
		properties.setShortcut(new QKeySequence(tr("Ctrl+P")));
		properties.setStatusTip(tr("G�rer les propri�t�s"));
		properties.triggered.connect(this,"displayProperties()");
		properties.setEnabled(false);
		
		
		topInvestissement = new QAction(new QIcon(rsrcPath + "/situation.png"), tr("&Classement Investissement"), this);
		topInvestissement.setShortcut(new QKeySequence(tr("Ctrl+N")));
		topInvestissement.setStatusTip(tr("Top des meilleurs investissements"));
		topInvestissement.triggered.connect(this,"displayTopInvestissements()");
		topInvestissement.setEnabled(false);
		

		aboutAct = new QAction(new QIcon(rsrcPath + "/about.png"), tr("A Propos de &JEditor"), this);
		aboutAct.setStatusTip(tr("A Propos de JEditor"));

		aboutQtAct = new QAction(new QIcon(rsrcPath + "/qt.png"), tr("A Propos de &Qt"), this);
		aboutQtAct.setStatusTip(tr("Show the Qt library's About box"));

	}
	
	public void displaySituation()
	{
		MainC.mainPannel.setCentralWidget(new IndexBox(true));
	
	}
	
	public void displayProperties()
	{
		MainC.mainPannel.setCentralWidget(new ProprietesWindow());
	}
	
	public void displayFinance()
	{
		MainC.mainPannel.setCentralWidget(new FinanceOptionWindow());
	}
	
	public void displayTopInvestissements()
	{
		MainC.mainPannel.setCentralWidget(new TopInvestissement());
	}

	private void createMenus()
	{

		fileMenu = menuBar().addMenu(tr("&Options"));
		fileMenu.addAction(financeOption);
		fileMenu.addAction(properties);
		fileMenu.addAction(situation);
		fileMenu.addAction(topInvestissement);

		helpMenu = menuBar().addMenu(tr("&Aide"));
		helpMenu.addAction(aboutAct);
		helpMenu.addAction(aboutQtAct);
	}

	/* Cr�ation de la barre de menu */
	private void createToolBars()
	{
		fileToolBar = addToolBar(tr("OptionBarre"));
		fileToolBar.addAction(situation);
		fileToolBar.addAction(financeOption);
		fileToolBar.addAction(properties);
		fileToolBar.addAction(topInvestissement);

	}

	private void createStatusBar()
	{
		statusBar().showMessage(tr("Pret"));
	}

	public void enableButton()
	{
		financeOption.setEnabled(true);
		properties.setEnabled(true);
		situation.setEnabled(true);
		topInvestissement.setEnabled(true);
	}
}
