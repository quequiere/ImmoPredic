package com.quequiere.predicimmo.gui.login;

import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.gui.windows.FinanceOptionWindow;
import com.quequiere.predicimmo.gui.windows.IndexBox;
import com.quequiere.predicimmo.gui.windows.ProprietesWindow;
import com.quequiere.predicimmo.network.BatimentHelper;
import com.quequiere.predicimmo.network.LoginHelper;
import com.quequiere.predicimmo.object.Profile;
import com.trolltech.qt.core.Qt.AlignmentFlag;
import com.trolltech.qt.core.Qt.AspectRatioMode;
import com.trolltech.qt.core.Qt.TransformationMode;
import com.trolltech.qt.gui.QApplication;
import com.trolltech.qt.gui.QComboBox;
import com.trolltech.qt.gui.QDialogButtonBox;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QLineEdit;
import com.trolltech.qt.gui.QPixmap;
import com.trolltech.qt.gui.QWidget;

public class LoginBox extends QWidget
{
	
	private String rsrcPath = "classpath:ressources";
	private QPixmap bandeau1 = new QPixmap(rsrcPath + "/player.jpg");
	private QPixmap bandeau2 = new QPixmap(rsrcPath + "/comingsoon.png");

	private QLineEdit loginLineEdit;
	private QLineEdit passwordLineEdit;
	private QLabel errorLabel;
	
	private QLabel bandeauImg ;
	private QComboBox liste;

	public LoginBox()
	{
		
		bandeau2= bandeau2.scaled(250, 170, AspectRatioMode.KeepAspectRatio,TransformationMode.SmoothTransformation);
		bandeau1= bandeau1.scaled(400, 250, AspectRatioMode.KeepAspectRatio,TransformationMode.SmoothTransformation);
		

		QGroupBox loginGroup = new QGroupBox(tr("Information login:"));

		QLabel loginLabel = new QLabel(tr("Pseudo:"));
		loginLineEdit = new QLineEdit();
		loginLineEdit.setFocus();

		QLabel passwordLabel = new QLabel(tr("Mot de passe:"));
		passwordLineEdit = new QLineEdit();
		passwordLineEdit.setEchoMode(QLineEdit.EchoMode.Password);
		passwordLineEdit.returnPressed.connect(this, "startLogin()");

		errorLabel = new QLabel(tr("Unknow error !"));
		errorLabel.setVisible(false);

		QGridLayout loginLayout = new QGridLayout();
		loginLayout.addWidget(loginLabel, 0, 0);
		loginLayout.addWidget(loginLineEdit, 0, 1);
		loginLayout.addWidget(passwordLabel, 1, 0);
		loginLayout.addWidget(passwordLineEdit, 1, 1);
		loginLayout.addWidget(errorLabel, 2, 1);
		loginGroup.setLayout(loginLayout);

		
		
		QGroupBox accountGroup = new QGroupBox(tr("Configuration serveur:"));

		QLabel serverLabel = new QLabel(tr("Serveur:"));

		liste = new QComboBox();
		liste.addItem("Monde V3 (beta)");
		liste.addItem("Coming soon !");
		
		liste.activated.connect(this, "changeServer()");
		
		
		bandeauImg = new QLabel();
		bandeauImg.setPixmap(bandeau1);
		bandeauImg.setAlignment(AlignmentFlag.AlignCenter);
		

		QGridLayout accountLayout = new QGridLayout();
		
		
		accountLayout.addWidget(serverLabel, 0, 0);
		accountLayout.addWidget(liste, 0, 1);
		accountLayout.addWidget(bandeauImg,1,0,1,-1);
	
		accountGroup.setLayout(accountLayout);
		

		QDialogButtonBox boutons = new QDialogButtonBox();
		boutons.addButton(QDialogButtonBox.StandardButton.Ok).clicked.connect(this, "startLogin()");
		boutons.addButton(QDialogButtonBox.StandardButton.Cancel).clicked.connect(QApplication.instance(), "quit()");

		
		QGridLayout layout = new QGridLayout();
		layout.addWidget(loginGroup, 0, 0);
		layout.addWidget(accountGroup, 1, 0);
		layout.addWidget(boutons, 2, 0);
		setWindowTitle(tr(" Account Configuration"));
		setLayout(layout);
	}
	
	public void changeServer()
	{
		if(liste.currentIndex()==0)
		{
			bandeauImg.setPixmap(bandeau1);
		}
		else
		{
			bandeauImg.setPixmap(bandeau2);
		}
		
	}

	public void startLogin()
	{
		System.out.println("Verify nickname");
		if (loginLineEdit.isModified() && loginLineEdit.displayText() != null && loginLineEdit.displayText().length() > 0)
		{
			System.out.println("nickname ok");
			System.out.println("Verify password");
			if (passwordLineEdit.isModified() && passwordLineEdit.displayText() != null && passwordLineEdit.displayText().length() > 0)
			{
				System.out.println("Password ok");
				errorLabel.setVisible(false);
				MainC.profile = new Profile(loginLineEdit.displayText());

				if (!MainC.profile.connect(passwordLineEdit.text()))
				{
					errorLabel.setText("Authentification impossible sur le serveur, verifiez votre pseudo et votre mot de passe !");
					errorLabel.setPalette(MainPannel.rouge);
					errorLabel.setVisible(true);
				}
				else
				{
					System.out.println("Connection detect�e valide !");
					
					MainC.mainPannel.setCentralWidget(new IndexBox(false));
					//PrintStream outStream = new PrintStream(new MyOutputStream(System.out), true);
					//System.setOut(outStream);
					
					QApplication.invokeLater(2,new Runnable() {
					    @Override
					    public void run() {
					    	LoginHelper.loadGeneralInformation();
							MainC.mainPannel.enableButton();
							BatimentHelper.loadBatiment();
					    }
					});
					
				}
			}
			else
			{
				errorLabel.setText("Merci d'entrer un mot de passe correcte !");
				errorLabel.setPalette(MainPannel.rouge);
				errorLabel.setVisible(true);
				
			}

		}
		else
		{
			errorLabel.setText("Merci de de renseigner un pseudo valide !");
			errorLabel.setPalette(MainPannel.rouge);
			errorLabel.setVisible(true);
		}
		
	
	
		
		
	}

}
