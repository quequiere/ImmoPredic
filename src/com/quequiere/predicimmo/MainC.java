package com.quequiere.predicimmo;

import com.gargoylesoftware.htmlunit.WebClient;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.gui.windows.FinanceOptionWindow;
import com.quequiere.predicimmo.gui.windows.IndexBox;
import com.quequiere.predicimmo.gui.windows.ProprietesWindow;
import com.quequiere.predicimmo.object.Profile;
import com.trolltech.qt.gui.QApplication;

public class MainC
{

	public static Profile profile;
	public static MainPannel mainPannel;
	public static final WebClient webClient = new WebClient();


	public static void main(String[] args)
	{

		QApplication.initialize(args);

		mainPannel = new MainPannel();
		mainPannel.resize(mainPannel.width(), mainPannel.height());
		mainPannel.setWindowTitle("PredicImmo Alpha by quequiere");
		mainPannel.show();

		QApplication.instance().exec();
	}
	
	public static String cleanString(String s)
	{
		String s2 = s.replace(" ", "");
		s2 = s2.replace("\r", "");
		s2 = s2.replace("\n", "");
		s2 = s2.replace("\t", "");
		return s2;
	}

}
