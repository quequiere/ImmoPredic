package com.quequiere.predicimmo.calculator.loyer;

import java.util.ArrayList;

import com.quequiere.predicimmo.network.BatimentHelper;
import com.quequiere.predicimmo.object.Batiment;

public class CalculatorLoyer
{
	private static ArrayList<Loyer> loyer = new ArrayList<Loyer>();
	
	
	
	public static void reloadLoyer()
	{
		System.out.println("Reloading loyer ...");
		loyer.clear();
		
		for(Batiment b:BatimentHelper.batiments)
		{
			if(b.getLoyer()!=-1)
			{
				System.out.println("Loading loyer for "+b.getName());
				loyer.add(new Loyer(b));
			}
		}
		
		System.out.println("Loyer reloaded !");
	}



	public static ArrayList<Loyer> getLoyer()
	{
		return loyer;
	}
	

	
}
