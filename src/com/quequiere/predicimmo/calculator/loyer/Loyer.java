package com.quequiere.predicimmo.calculator.loyer;

import com.quequiere.predicimmo.object.Batiment;

public class Loyer
{
	private Batiment batiment;

	public Loyer(Batiment b)
	{
		batiment=b;
	}
	
	
	
	public Batiment getBatiment()
	{
		return batiment;
	}



	public double getDailyIncome()
	{
		return batiment.getFinalLoyer();
	}
	
	public int getTimeBeforeRefund()
	{
		return (int) Math.ceil(batiment.getPriceAchat()/this.getDailyIncome());
	}
	
	public double getInvestissement()
	{
		return batiment.getPriceAchat();
	}

}
