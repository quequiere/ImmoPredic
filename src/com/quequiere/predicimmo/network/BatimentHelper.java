package com.quequiere.predicimmo.network;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.calculator.loyer.CalculatorLoyer;
import com.quequiere.predicimmo.object.Batiment;
import com.quequiere.predicimmo.object.BatimentType;
import com.quequiere.predicimmo.object.BatimentTypeEntreprise;
import com.quequiere.predicimmo.object.Profile;

public class BatimentHelper
{
	public static ArrayList<Batiment> batiments = new ArrayList<Batiment>();

	private static String persoImmo = "http://mondebeta.empireimmo.com/properties/build.php";
	private static String entrepriseImmo = "http://mondebeta.empireimmo.com/entreprise/immobilisation_build.php";

	private static String persoImmoPostRequest = "http://mondebeta.empireimmo.com/agency/agency_ajax.php";
	private static String entrepriseImmoPostRequest = "http://mondebeta.empireimmo.com/entreprise/marche_ajax.php";

	public static void loadEmbellissement()
	{
		try
		{
			HtmlPage page = MainC.webClient.getPage(Profile.isEntreprise ? entrepriseImmo : persoImmo);
			MainC.webClient.waitForBackgroundJavaScript(2000);
			final HtmlTable table = page.getFirstByXPath("//table");

			Batiment last = null;
			boolean previousHasNext = false;

			for (int x = 1; x < table.getRowCount(); x++)
			{
				final HtmlTableRow row = table.getRow(x);
				Batiment b = new Batiment(row, Profile.isEntreprise);

				if (previousHasNext)
				{
					last.setNextBatiment(b);
					if (last != null)
					{
						b.setPreviousBatiment(last);
					}

				}

				if (b.isEmbellissementPossible())
				{
					last = b;
					previousHasNext = true;
				}
				else
				{
					previousHasNext = false;
					b.setPreviousBatiment(last);
					last = null;
				}

				batiments.add(b);
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	}

	public static Batiment getByName(String name)
	{

		for (Batiment b : batiments)
		{
			try
			{
				
				if(name.equals(b.getName()))
				{
					return b;
				}

				if (name.contains("(-"))
				{
					String[] tmp = name.split(" \\(-");
					name = tmp[0];
					double percent = Double.parseDouble(tmp[1].split("%")[0]);
					System.out.println("==> reduction: " + percent + " %");
				}

				
				if (b.getName().replace(" ", "").contains(name))
				{
					return b;
				}
				else if (b.getName().replace(" ", "").contains(new String(name.getBytes("iso-8859-1"), "utf8")))
				{
					return b;
				}
				

			}
			catch (UnsupportedEncodingException e)
			{

				e.printStackTrace();
			}

		}
		return null;
	}

	public static void loadMarketPrice() throws UnsupportedEncodingException
	{

		for (Enum<? extends Enum<?>> type : Profile.isEntreprise ? BatimentTypeEntreprise.values() : BatimentType.values())
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "list"));
			params.add(new NameValuePair("type", type.name()));

			try
			{
				HtmlPage page = PostHelper.post(new URL(Profile.isEntreprise ? entrepriseImmoPostRequest : persoImmoPostRequest), params);
				MainC.webClient.waitForBackgroundJavaScript(2000);
				final HtmlTable table = page.getFirstByXPath("//table");

				for (int x = 1; x < table.getRowCount(); x++)
				{
					final HtmlTableRow row = table.getRow(x);
					String name = row.getCell(0).asText();
					boolean find = false;

					for (Batiment b : batiments)
					{

						if (name.contains("(-"))
						{
							System.out.println(name);
							String[] tmp = name.split(" \\(-");
							name = tmp[0];
							double percent = Double.parseDouble(tmp[1].split("%")[0]);
							System.out.println("==> reduction: " + percent + " %");
						}

						if (b.getName().contains(name))
						{

							b.updateBuyPrice(row, (BatimentType) type);
							find = true;
							break;
						}
						else if (b.getName().contains(new String(name.getBytes("iso-8859-1"), "utf8")))
						{
							b.updateBuyPrice(row, (BatimentType) type);
							find = true;
							break;
						}
					}

					if (!find)
					{
						System.out.println("--------------------");
						System.out.println("Pas de correspondance pour " + name);
						System.out.println("--------------------");
					}

				}

			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
		}

	}

	public static void loadBatiment()
	{
		batiments.clear();
		loadEmbellissement();
		try
		{
			loadMarketPrice();
			CalculatorLoyer.reloadLoyer();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		System.out.println("==== FIN LOADING =====");
	}
}
