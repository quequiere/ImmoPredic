package com.quequiere.predicimmo.network;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.gui.MainPannel;
import com.quequiere.predicimmo.gui.windows.IndexBox;

public class LoginHelper
{
	public static boolean connect(String username,String password)
	{

		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

		MainC.webClient.getOptions().setJavaScriptEnabled(false);
		MainC.webClient.getOptions().setThrowExceptionOnScriptError(false);
		MainC.webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

		try
		{

			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("txtLogin", username));
			params.add(new NameValuePair("txtPassword", password));

			HtmlPage page = PostHelper.post(new URL("http://mondebeta.empireimmo.com/login.php?a=in&r=/home.php"), params);
			MainC.webClient.waitForBackgroundJavaScript(2000);

			if (page.asXml().contains("<div id=\"EIProfilName\">"))
			{
				System.out.println("Authentification du site ok !");
				return true;
			}
			else if(page.asXml().contains("Oups... Probl�me d'identification�!"))
			{
				System.out.println("Connection �chou�e d�tail pseudo:"+username+" password: "+password );
			}
			else
			{
				System.out.println("Can't find error.");
				System.out.println(page.asXml());
			}

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		return false;

	}
	
	
	
	
	public static void loadGeneralInformation()
	{
		long money = -1;
		int points = -1;
		int level =-1;
		int place =-1;
		long caisseSolidaire =-1;
		long pret =-1;
		try
		{
			HtmlPage page = MainC.webClient.getPage("http://mondebeta.empireimmo.com/home.php");
			MainC.webClient.waitForBackgroundJavaScript(2000);
		
			String all = page.asText();
			all = all.split(MainC.profile.getUsername())[1];
			all = all.split(" �")[0];
			all = MainC.cleanString(all);
			money=Long.parseLong(all);
			System.out.println("Money loaded: "+money);
		
			
			all = page.asText();
			all=all.split("Points : ")[1].split("Place")[0];
			all = MainC.cleanString(all);
			points=Integer.parseInt(all);
			System.out.println("Points loaded: "+points);
			
			all = page.asText();
			all=all.split("Niveau : ")[1].split("Propri�t�s")[0];
			all = MainC.cleanString(all);
			level=Integer.parseInt(all);
			System.out.println("Level loaded: "+level);
			
			all = page.asText();
			all=all.split("Place : ")[1].split("�me")[0];
			all = MainC.cleanString(all);
			place=Integer.parseInt(all);
			System.out.println("Place loaded: "+place);
			
			
			
			
			
			
			page = MainC.webClient.getPage("http://mondebeta.empireimmo.com/finance/");
			MainC.webClient.waitForBackgroundJavaScript(2000);
			all = page.asText();
		
			all = all.split("Caisse Solidaire ")[1];
			all = all.split(" �")[0];
			all = MainC.cleanString(all);
			caisseSolidaire=Long.parseLong(all);
			System.out.println("Caisse solidaire loaded: "+caisseSolidaire);
			
			
			all = page.asText();
			all = all.split("Mensualit� des pr�ts ")[1];
			all = all.split(" �")[0];
			all = MainC.cleanString(all);
			pret=Long.parseLong(all);
			System.out.println("Pr�ts: "+pret);
			
			
			

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	
		MainC.profile.setFond(money);
		MainC.profile.setLevel(level);
		MainC.profile.setPlace(place);
		MainC.profile.setPoints(points);
		MainC.profile.setCaisseSolidaire(caisseSolidaire);
		MainC.profile.setPret(pret);
		MainC.mainPannel.setCentralWidget(new IndexBox(true));
		
		
		
	}
}
