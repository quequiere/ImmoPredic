package com.quequiere.predicimmo.network;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.predicimmo.MainC;


public class PostHelper
{
	public static HtmlPage post(URL url, List<NameValuePair> params)
	{

		WebRequest requestSettings = new WebRequest(url, HttpMethod.POST);

		requestSettings.setAdditionalHeader("Accept", "text/plain, */*; q=0.01");
		requestSettings.setAdditionalHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		requestSettings.setAdditionalHeader("Referer", ""+url.getPath());
		requestSettings.setAdditionalHeader("Accept-Language", "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4");
		requestSettings.setAdditionalHeader("Accept-Encoding", "gzip,deflate,sdch");
		requestSettings.setAdditionalHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");
		requestSettings.setAdditionalHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
		requestSettings.setAdditionalHeader("X-Requested-With", "XMLHttpRequest");
		
		
		requestSettings.setRequestParameters(params);
		try
		{
			return MainC.webClient.getPage(requestSettings);
		}
		catch (FailingHttpStatusCodeException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
