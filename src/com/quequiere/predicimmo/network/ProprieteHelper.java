package com.quequiere.predicimmo.network;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.predicimmo.MainC;
import com.quequiere.predicimmo.object.Batiment;
import com.quequiere.predicimmo.object.Profile;
import com.quequiere.predicimmo.object.Propriete;
import com.quequiere.predicimmo.object.StatutType;
import com.trolltech.qt.gui.QApplication;

public class ProprieteHelper
{
	private static String persoImmo = "http://mondebeta.empireimmo.com/properties/ajax.php";
	private static String entrepriseImmo = "todev";

	public static ArrayList<Propriete> reloadPropriete()
	{
		ArrayList<Propriete> prop = new ArrayList<Propriete>();
		
		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("action", "search"));
			params.add(new NameValuePair("type", ""));
			params.add(new NameValuePair("status", ""));
			params.add(new NameValuePair("lifetime", "120"));
			params.add(new NameValuePair("building", ""));
			params.add(new NameValuePair("town", ""));
			params.add(new NameValuePair("level", ""));
			params.add(new NameValuePair("pager_index", "0"));
			params.add(new NameValuePair("pager_size", "10000"));
			params.add(new NameValuePair("sort", "6"));
			params.add(new NameValuePair("asc", "1"));
		
			
			HtmlPage page = PostHelper.post(new URL(Profile.isEntreprise ? entrepriseImmo : persoImmo), params);
		
			//System.out.println(page.asXml());
		
			MainC.webClient.waitForBackgroundJavaScript(2000);
			
		
			final HtmlTable table = page.getFirstByXPath("//table");
			if(table==null)
				return prop;


			for (int x = 1; x < table.getRowCount(); x=x+2)
			{
				final HtmlTableRow row = table.getRow(x);
				final HtmlTableRow row2 = table.getRow(x+1);
				String pres = MainC.cleanString(row.asXml());
				String detail = MainC.cleanString(row2.asXml());

				String idString = pres.split("id=\"prop_")[1].split("\"><td><inputtype")[0];
				int id = Integer.parseInt(idString);
				
				//System.out.println(pres);
				
				String batimentTypeString = pres.split("</td><td>")[1].split("</td><tdclass")[0];
				Batiment b = BatimentHelper.getByName(batimentTypeString);
			
			
				String batimentDegradationString = pres.split("gradation:")[1].split("%\">")[0].replace(",", ".");
				double degradation = Double.parseDouble(batimentDegradationString);
				
				StatutType statut = null;
				for(StatutType s:StatutType.values())
				{
					if(pres.contains(s.name()))
					{
						statut=s;
						break;
					}
				}
				prop.add(new Propriete(b, id, degradation, statut));
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
		return prop;
	}
}
