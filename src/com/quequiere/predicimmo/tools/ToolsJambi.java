package com.quequiere.predicimmo.tools;

import java.util.ArrayList;

import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QGridLayout;
import com.trolltech.qt.gui.QGroupBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QLayoutItem;
import com.trolltech.qt.gui.QLayoutItemInterface;
import com.trolltech.qt.gui.QPalette;
import com.trolltech.qt.gui.QVBoxLayout;
import com.trolltech.qt.gui.QWidget;
import com.trolltech.qt.gui.QPalette.ColorRole;

public class ToolsJambi
{
	
	public static void setColorGroup(QGroupBox box,QColor qcolor)
	{
		QPalette pal = box.palette();
		pal.setColor(ColorRole.Window, qcolor);
		box.setAutoFillBackground(true);
		box.setPalette(pal);
	}

	public static void clearGrid(QVBoxLayout generalGroupGridSave)
	{
		ArrayList<QWidget> toremove = new ArrayList<>();
		for (int row = 0; row < generalGroupGridSave.count(); row++)
		{
			QLayoutItemInterface wid = generalGroupGridSave.itemAt(row);
			if (wid != null && wid instanceof QLayoutItem)
			{
				QLayoutItem item = (QLayoutItem) wid;
				item.widget().setVisible(false);
				toremove.add(item.widget());
			}

		}

		for (QWidget w : toremove)
		{
			generalGroupGridSave.removeWidget(w);
		}

	}

	public static void clearGrid(QGridLayout generalGroupGridSave)
	{
		ArrayList<QWidget> toremove = new ArrayList<>();
		for (int row = 0; row < generalGroupGridSave.rowCount(); row++)
		{
			for (int col = 0; col < generalGroupGridSave.columnCount(); col++)
			{
				QLayoutItemInterface wid = generalGroupGridSave.itemAtPosition(row, col);
				if (wid != null && wid instanceof QLayoutItem)
				{
					QLayoutItem item = (QLayoutItem) wid;
					QLabel widgetok = (QLabel) item.widget();
					widgetok.setVisible(false);
					toremove.add(widgetok);
				}

			}
		}

		for (QWidget w : toremove)
		{
			generalGroupGridSave.removeWidget(w);
		}

	}

}
